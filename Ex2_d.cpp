/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex2 d of lab6
 * Task		   : Ma tran thua		
 * Reference   :
 * Date        : 21/5/2015
 */

#include<iostream>
#include<fstream>
#include<sstream>

using namespace std;

struct Pair{
	float x = 0;
	float y = 0;
};

class SparseMatrix{
	private:
		int colNum, rowNum;
		Pair data[][100];
	public:
		SparseMatrix(){
			colNum = rowNum = 0;
		}
		void setSize(int m, int n);
		void setData(float _data[][100], int m, int n);
		void input(char* fileName);
		void output(char* fileName);
};

void SparseMatrix::setSize(int m, int n){
	rowNum = m;
	colNum = n;
}

void SparseMatrix::setData(float _data[][100], int m, int n){
	int k = 0;
	setSize(m, n);
	for ( int i = 0; i < m; i++){
		for ( int j = 0; j < n; j++){
			if(_data[i][j] != 0){
				data[i][k].x = j;
				data[i][k].y = _data[i][j];
				k++;
			}

		}
		k = 0;
	}
}

void SparseMatrix::input(char* fileName){
	ifstream is(fileName);
	bool check[100][100];
	int m = 0;
	int n = 0;
	float data[100][100];
	if(!is.fail()){
		string line;
		while(getline(is, line)){
			istringstream iss(line);
			float b;
			int a;
			char c;
			while (iss >> a >> c >> b){	
				data[m][a] = b;
				check[m][a] = true;
				if (a > n) n = a;
			}
			m++;
		}
		n++;
		
		for ( int i = 0; i < m; i++){
			for ( int j = 0; j <n; j++){
				if (check[i][j] != true ) data[i][j] = 0;
			}
		}
		
		setData(data, m, n);
	} else{
		cout << "Can't read file." << endl;
	}
}

void SparseMatrix::output(char* fileName){
	ofstream os(fileName);
	if(!os.fail()){
		//int j = 0;
		for ( int i = 0; i < rowNum; i++){
			for( int j = 0; j < colNum; j++){
				os << data[i][j].x << ":" << data[i][j].y << " ";
				cout << data[i][j].x << ":" << data[i][j].y << " ";
			}
			os << endl;
		}
	} else {
		cout << "Can't write file." << endl;
	}
}

int main(){
	SparseMatrix* sm = new SparseMatrix();
	sm->input("ex2_b.txt");
	sm->output("ex2_d.txt");
	return 0;
}
