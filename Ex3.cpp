/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex3 d of lab6
 * Task		   : Ma tran thua		
 * Reference   :
 * Date        : 21/5/2015
 */

#include<iostream>
#include<fstream>
#include<sstream>

using namespace std;

class Diem{
	private:
		char* tenMonHoc;
		float diemSo;
	public:
		Diem(){
			tenMonHoc = NULL;
			diemSo = 0; 
		}
		Diem(char* _ten, float _diem){
			tenMonHoc = _ten;
			diemSo = _diem;
		}
		~Diem(){
			delete [] tenMonHoc;
			diemSo = 0;
		}
		void print(){
			cout << tenMonHoc << " " << diemSo;
		}
		Diem* operator=(Diem *a){
			tenMonHoc = a->tenMonHoc;
			diemSo = a->diemSo;
			return this;
		}
};

struct Node{
	Diem diem;
	Node *next;
};



class DanhSachDiem{
	private:
		Node* list;
		Node *head;
		Node *tail;
	public:
		void addNode(char* tenMon, float diem);
		void print();
		Node* getHead(){
			return head;
		}
		void setHead(Node* _head){
			
		}
};

Node* createNode(char* tenMon, float diem){
	Node* p = new Node();
	Diem d(tenMon, diem);
	p->diem = d;
	p->next = NULL;
}

void DanhSachDiem::addNode(char* tenMon, float diem){
	Node* p = createNode(tenMon, diem);
	if(head == NULL && tail == NULL){
		head = p;
		tail = p;
	} else {
		tail->next = p;
		tail = p;
	}
}

void DanhSachDiem::print(){
	Node *p = head;
	while(p != NULL){
		(p->diem).print();
		cout << endl;
	} 
}

class SinhVien{
	private:
		char* ten;
		int tuoi;
		DanhSachDiem diem;
	public:
		SinhVien(char* _ten, int _tuoi, DanhSachDiem _diem);
	//	~SinhVien();
		void print(){
			cout << "Ten: " << ten << endl;
			cout << "Tuoi: " << tuoi << endl;
			diem.print();
			cout << endl; 
		}
};

SinhVien::SinhVien(char* _ten, int _tuoi, DanhSachDiem _diem){
	ten = _ten;
	tuoi = _tuoi;
	diem.getHead()->diem = _diem.getHead()->diem;
	diem.getHead()->next = _diem.getHead()->next;
}

int main(){
	DanhSachDiem dsd;
	dsd.addNode("Toan", 10);
	dsd.addNode("Ly", 9);
	dsd.addNode("Hoa", 8);
	SinhVien sv("Nguyen Van A", 13, dsd);
	sv.print();
	return 0;
}
