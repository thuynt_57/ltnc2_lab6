/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex2 of lab6
 * Task		   : Ma tran thua		
 * Reference   :
 * Date        : 21/5/2015
 */

#include<iostream>
#include<fstream>
#include<sstream>

using namespace std;


void input_a(char* fileName, int &m, int &n, float data[][100]){
	ifstream is(fileName);
	if(!is.fail()){
		is >> m >> n;
		for (int i = 0; i < m; i++){
			for (int j = 0; j < n; j++){
				data[i][j] = 0;
			}
		}
		
		string line;
		int k = -1; // chiu khong hieu noi cho nay :/
		while(getline(is, line)){
			istringstream iss(line);
			float b;
			int a;
			char c;
			while (iss >> a >> c >> b){	
				data[k][a] = b;
			}
			k++;
		}
	}
}

// b
void input_b(char* fileName, int &m, int &n, float data[][100]){
	ifstream is(fileName);
	bool check[100][100];
	m = 0;
	n = 0;
	if(!is.fail()){
		string line;
		while(getline(is, line)){
			istringstream iss(line);
			float b;
			int a;
			char c;
			while (iss >> a >> c >> b){	
				data[m][a] = b;
				check[m][a] = true;
				if (a > n) n = a;
			}
			m++;
		}
		n++;
		
		for ( int i = 0; i < m; i++){
			for ( int j = 0; j <n; j++){
				if (check[i][j] != true ) data[i][j] = 0;
			}
		}
	} else{
		cout << "Can't read file." << endl;
	}
}

int main(){
	cout << "Test for a" << endl;
	int m, n;
	float data[100][100];
	input_a("ex2_a.txt", m, n, data);
	cout << m << " " << n << endl;
	for (int i = 0; i < m; i++){
		for (int j = 0; j < n; j++){
			cout << data[i][j] << " ";
		}
		cout << endl;
	}
	
	cout << "Test for b" << endl;
	int mb, nb;
	float b[100][100];
	input_b("ex2_b.txt", mb, nb, b);
	cout << mb << " " << nb << endl;
	for (int i = 0; i < mb; i++){
		for (int j = 0; j < nb; j++){
			cout << b[i][j] << " ";
		}
		cout << endl;
	}	
	return 0;
}
