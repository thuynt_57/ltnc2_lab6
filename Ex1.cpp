/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing 
 * Description : Ex1 of lab6
 * Task		   : Ma tran la mot bang so 2 chieu. Trong bai nay se cai dat cac ham doc ma tran va in ra ma tran 
 				ra file theo dinh dang cho truoc
 				a, Viet ham nhap ma tran tu 1 file co dinh dang nhu sau
 				- Dong dau tien c� 2 so M, N l� so dong, so cot cua ma tran
 				- M dong tiep theo moi dong co N so thuc la cac dong cua ma tran
 				- Cac so cach nhau boi dau cach
 				b, Viet ham nhap ma tran tu file co dinh dang nhu sau
 				- Moi dong cua file la mot dong cua ma tran
 				- So so tren moi dong bang nha va bang cot cua ma tran
 				- Cac so cach nhau boi dau cach
 				c, Viet ham nhap ma tran tu file nhu phan b nhung cac so cach nhau boi dau phay(CSV)
 				d, Viet ham in ma tran theo dinh dang CSV
 				e, Nem ngoai le tu cac ham tren khi phat hien loi nhap lieu(file khong dung dinh dang)
 				
 * Reference   :
 * Date        : 15/5/2015
 */

#include<iostream>
#include<fstream>
#include<string.h>
#include<sstream>

using namespace std;

// a
void input_a(char* fileName, int &m, int &n, float data[][100]){
	ifstream is(fileName);

	if(!is.fail()){
		is >> m;
		is >> n;
		
	//	data = new float*[m];
	    	
		for( int i = 0; i < m; i++ ){
	//		data[i] = new float[n];
			for( int j = 0; j < n; j++ ){
				is >> data[i][j];
			}
		}
	}
	is.close();
}

// b
void input_b(char* fileName, int &m, int &n, float data[][100]){
	ifstream is(fileName);
	string line;
	int i = 0;
	int j = 0;
	while (getline(is, line)){
		istringstream iss(line);
		while ( iss >> data[i][j]){
			j++;
		}
		n = j;
		j = 0;
		i++;
	}
	m = i;
}

// c
void input_c(char* fileName, int &m, int &n, float data[][100]){
	ifstream is(fileName);
	string line;
	int i = 0;
	int j = 0;
	char c;
	while (getline(is, line)){
		istringstream iss(line);
		while (iss >> data[i][j]){
			iss >> c;
			j++;
		}
		n = j;
		j = 0;
		i++;
	}
	m = i;
}

// d
void printCSV(float data[][100], int m, int n){
	for (int i = 0; i < m; i++){
		for (int j = 0; j < n - 1; j++){
			cout << data[i][j] << ", ";
		}
		cout << data[i][n-1];
		cout << endl;
	}
}

int main(){
	cout << "Test for a" << endl;
	float a[100][100];
	int m, n;
	input_a("ex1_a.txt", m, n, a);
	cout << m << " " << n << endl;
//	a = new float*[m];
	for (int i = 0; i < m; i++){
//		a[i] = new float[n];
		for (int j = 0; j < n; j++){
			cout << a[i][j] << " ";
		}
		cout << endl;
	}

	cout << "Test for b" << endl;
	float b[100][100];
	int mb, nb;
	input_b("ex1_b.txt", mb, nb, b);
	cout << mb << " " << nb << endl;
	for (int i = 0; i < mb; i++){
		for (int j = 0; j < nb; j++){
			cout << b[i][j] << " ";
		}
		cout << endl;
	}
	
	cout << "Test for c" << endl;
	float c[100][100];
	int mc, nc;
	input_c("ex1_c.txt", mc, nc, c);
	printCSV(c, mc, nc);
	
	
	
	return 0;
}

